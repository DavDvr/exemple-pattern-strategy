<?php

namespace ExerciceStrategy\src\strategies;

class ComportementArcEtFleche implements ComportementArme
{

    public function utiliserArme(): void
    {
        echo 'Je combat en utilisant un Arc et des Flèches ...<br>';
    }
}