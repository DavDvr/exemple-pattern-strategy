<?php

namespace ExerciceStrategy\src\strategies;

class ComportementPoignard implements ComportementArme
{

    public function utiliserArme(): void
    {
        echo 'Je combat en utilisant un Poignard ...<br>';
    }
}