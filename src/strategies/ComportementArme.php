<?php
namespace ExerciceStrategy\src\strategies;

interface ComportementArme
{
    public function utiliserArme(): void;
}