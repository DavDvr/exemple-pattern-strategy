<?php

namespace ExerciceStrategy\src\strategies;

class ComportementHache implements ComportementArme
{

    public function utiliserArme(): void
    {
       echo 'Je combat en utilisant une Hâche ...<br>';
    }
}