<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Pattern Strategy </title>
</head>
<body>
<?php

use ExerciceStrategy\src\personnages\Reine;
use ExerciceStrategy\src\personnages\Roi;
use ExerciceStrategy\src\personnages\Troll;
use ExerciceStrategy\src\strategies\ComportementPoignard;

require_once (dirname(__DIR__)."/vendor/autoload.php");

define('BR','<br>');

echo '____________________________Reine'.BR;
    $reine  = new Reine();
    $reine->combatre();
echo '__________________________'.BR;
    $reine->combatre();
echo '__________________________'.BR;
    $reine->combatre();
echo '__________________________'.BR;
    $reine->combatre();
echo '__________________________'.BR;
    $reine->combatre();

echo '____________________________Roi'.BR;
    $roi  = new Roi();
    $roi->combatre();
    echo "J'utilisais l'arme par defaut mais ca ne me plait pas ...".BR;
    echo BR;
    $roi->setComportementArme(new ComportementPoignard());
    $roi->combatre();
    echo BR;


echo '____________________________Troll'.BR;
    $troll  = new Troll();
    $troll->combatre();
?>
</body>
</html>