<?php

namespace ExerciceStrategy\src\personnages;

use ExerciceStrategy\src\strategies\ComportementArcEtFleche;
use ExerciceStrategy\src\strategies\ComportementHache;

class Reine extends Personnage
{
    protected int $score;

    /**
     * La Reine a un ComportementArme par defaut qui est ComportementArcEtFleche
     * Reine constructor.
     * @param int $score
     */
    public function __construct($score=0)
    {
        $this->comportementArme = new ComportementArcEtFleche();
        $this->score = $score;
    }

    public function combatre(): void
    {
        $this->score += 4;
        echo 'Je suis une Reine...<br>';
        $this->comportementArme->utiliserArme();
        echo "Le score de la Reine est de: " .$this->score. ' points<br>';

        if ($this->score > 15)
        {
            $this->setComportementArme(new ComportementHache());

        }
    }
}