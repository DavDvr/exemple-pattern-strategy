<?php

namespace ExerciceStrategy\src\personnages;

use ExerciceStrategy\src\strategies\ComportementArme;

abstract class Personnage
{
    protected string $name;
    protected int $score;

    protected ComportementArme $comportementArme;

    public abstract function combatre():void;

    /**
     * @param ComportementArme $comportementArme
     */
    public function setComportementArme(ComportementArme $comportementArme): void
    {
        $this->comportementArme = $comportementArme;
    }

}