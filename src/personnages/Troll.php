<?php

namespace ExerciceStrategy\src\personnages;

use ExerciceStrategy\src\strategies\ComportementPoignard;

class Troll extends Personnage
{

    protected int $score;

    /**
     * Troll constructor.
     * @param int $score
     */
    public function __construct(int $score=0)
    {
        $this->comportementArme = new ComportementPoignard();
        $this->score = $score;
    }

    public function combatre(): void
    {
        $this->score += 1;
        echo 'Je suis le Troll <br>';
        $this->comportementArme->utiliserArme();
        echo "Le score du Troll est de: " .$this->score. ' points';
    }
}