<?php

namespace ExerciceStrategy\src\personnages;

use ExerciceStrategy\src\strategies\ComportementHache;

class Roi extends Personnage
{

    protected int $score;

    /**
     * Roi constructor.
     * @param int $score
     */
    public function __construct(int $score = 0)
    {
        $this->comportementArme = new ComportementHache();
        $this->score = $score;
    }

    public function combatre(): void
    {
        $this->score += 2;
        echo 'Je suis un Roi <br>';
        $this->comportementArme->utiliserArme();
        echo "Le score du Roi est de: " .$this->score. ' points<br>';
    }
}